import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

//        System.out.println(dichotomique(80));
        int[] tab1 = {1, 23, 12, 9, 30, 2, 50};
        int[] tab2 = {11, 5, 12, 9, 44, 17, 2};
        int[] tabMaster = {7, 0, 6, 4};

        /*
        jeu du pendu, mot à trouver en parametre
         */
//        pendu("imparidigite");

        /*
        Entrer l'année de naissance en paramètre.
         */
//        System.out.println(age(1991));

//        participation();

        /*
        Chercher une nombre avec une boucle for dans ce tableau {10, 20, 50, 60}, nombre recherché en parametre
         */
//        System.out.println(chercheForNombre(20));

        /*
        Meme chose avec une boucle While
         */
//       System.out.println(chercheWhileNombre(20));

        /*
        Meme chose avec boucle Do-While
         */
//       System.out.println(chercheWhileDoNombre(60));

        /*
        Additionner les entier de 0 à n, n est l'entier en parametre.
         */
//        System.out.println(somme(60));

        /*
        Calcul de la factorielle en parametre
         */
//        System.out.println(factorielle(10));

        /*
        FizzBuzz, integer en parametre
         */
//        fizz(60);

        /*
        Iteration avec ce tableau {12.2, 11.3, 4.4, 2.0}
         */
//        iteration();

        /*
        Somme entre deux index, indexes en parmetres. Dans ce tableau : {4, 6, 2, 2}
         */
//        somme(1,2);

        /*
        Moyenne, min, max
         */
//        moyenne();

        /*
        Somme d'un tableau 2d : {{4, 6, 5}, {7, 11, 34}, {28, 6, 2}, {1, 2, 3}, {4, 3, 10}}, resultat 126
         */
//        System.out.println(sommeTableau());

        /*
        Addition sur une diagonale sur ce tableau 2d {{4, 6, 5, 2}, {7, 11, 34, 1}, {28, 6, 2, 15}, {1, 2, 3, 19}}
         */
//        System.out.println(diag());

        /*
        Pyramide v1, nombre d'étoiles en entrée
         */
//        pyramideV1(5);

        /*
        Pyramide v2
         */
//        pyramideV2(5);

        /*
        Decompte de voyelles, mot en parametre
         */
//        System.out.println(voyelles("bonjour"));

        /*
        Decompte de majuscule, mot en parametre
         */
//        System.out.println(majuscule("bonJour"));

        /*
        Minuscule en maj, mot en parametre
         */
//        System.out.println(minToMaj("bonjour"));

        /*
        Inversion d'une chaine de caracteres, mot en parametre
         */
//        System.out.println(inverse("jaime les glaces"));

        /*
        CamelCase
         */
//        System.out.println(camelCase("J'aime les glaces"));

        /*
        Localisation d'une chaine de caracteres
         */
//        System.out.println(localisation("J'aime les glaces", "glaces"));

        /*
        Conversion binaire vers base 10
         */
//        conversionBinToTen();

        /*
        base 10 vers binaire, nombre en base10 en parametre
         */
//        System.out.println(tenToBin(125));

        /*
        fusion de tableaux {1, 3, 4, 5} {2, 4, 6, 8};
         */
//        System.out.println(fusion());

        /*
        palindrome
         */
//        System.out.println(palindromeTwo("mon nom"));

        /*
        luhn
         */
//        System.out.println(cardVerifier("4263 9826 4026 9299"));

        /*
        Glouton [200, 100, 50, 20, 10, 5, 2, 1]
         */
//        System.out.println(glouton(125));

        /*
        Addition des nombres dun tableau en recursif dans ce tab : {2, 7, 12, 15, 22, 18, 25, 7, 8}, ne pas changer les parametres
         */
//        triRecursif(0,0);

        /*
        Chiffrement par décalage
         */
//        System.out.println(encrypt("balthazar au cimetierre", 2, "RIGHT"));

        /*
        Dechiffrement par decalage
         */
//        System.out.println(decrypt("dcnvjcbctVccwVekogvkgttg", 2, "RIGHT"));

        /*
        tri a bulle
         */
//        triBulle(tab1);

        /*
        tri par selection avec ce tableau {12,24,10,5,5,7,54,8,65,41,2,3}
         */
//        triSelection();

        /*
        Recherche dichotomique dans ce tableau {8,12,14,19,21,22,24,32,53,53,58,70,74,80,85,87,89,89,94,96}
         */
//        System.out.println(dichotomique(19));

        /*
        k  plus grand
         */
//        System.out.println(kPlusGrand(tab1, 3));

        /*
        mastermind, recursif : ne pas changer les parametres
         */
//        mastermind(1,0);
    }

    public static String age(Integer truc) {
        String est_majeur = "";
        if (2023 - truc >= 18) {
            est_majeur = "Tu es majeur, ton age : " + String.valueOf(2023 - truc);
        } else {
            est_majeur = "Non, ton age : " + String.valueOf(2023 - truc);
        }
        return est_majeur;
    }

    public static String participation() {
        int taux = 0;
        String reponse = "";
        Scanner scan = new Scanner(System.in);
        System.out.println("Celibataire? (O/N) ");
        String celib = scan.next();
        System.out.println("Salaire?");
        int salaire = scan.nextInt();
        System.out.println("Enfant? (Nombre)");
        int enfant = scan.nextInt();
        if (celib.equals("O")) {
            taux = 20;
        } else {
            taux = 25;
        }
        if (salaire < 1800) {
            taux = taux + 10;
        }
        taux = taux + enfant * 15;
        if (taux > 50) {
            taux = 50;
        }
        reponse = "Votre taux de particaption est de " + String.valueOf(taux) + "%";
        System.out.println(reponse);
        System.out.println("Voulez-vous continuer? (O/N)");
        String continuer = scan.next();
        if (continuer.equals("O")) {
            System.out.println(participation());
        } else {
            return "Terminé!";
        }
        return reponse;
    }

    public static String chercheForNombre(int value) {
        int[] tableau = {10, 20, 50, 60};
        int tablength = tableau.length;
        int index = -1;
        for (int i = 0; i < tablength; i++) {
            if (tableau[i] == value) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return "Valeur non trouvée";
        }
        return "Valeur " + String.valueOf(value) + " trouvée à l'index : " + String.valueOf(index);
    }

    public static String chercheWhileNombre(int value) {
        int[] tableau = {10, 20, 50, 60};
        int tablength = tableau.length;
        int i = 0;
        int index = -1;
        while (i <= tablength) {
            if (tableau[i] == value) {
                index = i;
                break;
            }
            i = i + 1;
        }
        if (index == -1) {
            return "Valeur non trouvée";
        }
        return "Valeur " + String.valueOf(value) + " trouvée à l'index : " + String.valueOf(index);
    }

    public static String chercheWhileDoNombre(int value) {
        int[] tableau = {10, 20, 50, 60};
        int tablength = tableau.length;
        int i = 0;
        int index = -1;
        do {
            if (tableau[i] == value) {
                index = i;
                break;
            }
            i++;
        } while (i < tablength);
        if (index == -1) {
            return "Valeur non trouvée";
        }
        return "Valeur " + String.valueOf(value) + " trouvée à l'index : " + String.valueOf(index);
    }

    public static String somme(int value) {
        int i = 0;
        int j = 0;
        int somme = 0;
        do {
            j = j + i;
            i++;
            System.out.println(j);
        } while (i < value + 1);

        return "la somme : " + String.valueOf(j);
    }

    public static String factorielle(int value) {
        int i = 0;
        int j = 1;
        int somme = 0;
        do {
            i++;
            j = i * j;
            System.out.println(j);
        } while (i < value);

        return "la somme : " + String.valueOf(j);
    }

    public static void fizz(int value) {
        int i = 1;
        do {
            i++;
            if (i % 3 == 0) {
                System.out.println("fizz");
            } else if (i % 5 == 0) {
                System.out.println("buzz");
            } else {
                System.out.println(i);
            }
        } while (i < value);
    }

    public static void iteration() {
        double[] tableau = {12.2, 11.3, 4.4, 2.0};
        for (double v : tableau) {
            System.out.println(v);
        }
    }

    public static void somme(int a, int b) {
        int[] tableau = {4, 6, 2, 2};
        int somme = 0;
        for (int i = a; i < b; i++) {
            somme += tableau[i];
        }
        System.out.println("somme : " + String.valueOf(somme));
    }

    public static void moyenne() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Combien d'entrées?");
        int entrees = scan.nextInt();
        int somme = 0;
        int moyenne = 0;
        int[] tableau = {};
        ArrayList<Integer> li = new ArrayList<>();
        for (int i = 0; i < entrees; i++) {
            System.out.println("Nombre" + String.valueOf(i + 1) + " : ");
            int nb = scan.nextInt();
            li.add(nb);
        }
        ;
        for (Integer integer : li) {
            somme += integer;
            moyenne = somme / entrees;
        }
        System.out.println("Le max est de : " + Collections.max(li));
        System.out.println("Le min est de : " + Collections.min(li));
        System.out.println("La moyenne est de : " + String.valueOf(moyenne));
    }

    public static String sommeTableau() {
        int[][] tableau = {{4, 6, 5}, {7, 11, 34}, {28, 6, 2}, {1, 2, 3}, {4, 3, 10}};
        int somme = 0;
        String resultat = "";
        for (int i = 0; i < tableau.length; i++) {
            for (int j = 0; j < 3; j++) {
                somme += tableau[i][j];
            }
        }
        resultat = "Resultat : " + String.valueOf(somme);
        return resultat;
    }

    public static String diag() {
        int[][] tableau = {{4, 6, 5, 2}, {7, 11, 34, 1}, {28, 6, 2, 15}, {1, 2, 3, 19}};
        int width = 4;
        int height = 4;
        int diagonalType = 1;
        int somme = 0;
        int j = 3;
        Scanner scan = new Scanner(System.in);
        String resultat = "";
        System.out.println("Diagonale? (1/2) ");
        String diag = scan.next();
        if (diag.equals("1")) {
            for (int i = 0; i < tableau.length; i++) {
                somme += tableau[i][i];
                System.out.println("Diagonale 1 : " + String.valueOf(tableau[i][i]));
            }
        } else {
            for (int i = 0; i < tableau.length; i++) {
                somme += tableau[i][j];
                System.out.println("Diagonale 2 : " + String.valueOf(tableau[i][j]));
                j--;
            }
        }

        resultat = "Resultat : " + String.valueOf(somme);
        return resultat;
    }

    public static void conversionBinToTen() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Nombre binaire : ");
        long nombreBinaire = scan.nextLong();
        String nbToString = Long.toString(nombreBinaire);
        int length = nbToString.length();
        int[] tableau = new int[length];
        long somme = 0;
        long j = length - 1;
        for (int i = 0; i < length; i++) {
            tableau[i] = Character.getNumericValue(nbToString.charAt(i));
        }

        for (int i = 0; i <= length - 1; i++) {
            somme += tableau[i] * Math.pow(2, j);
            j--;
        }
        System.out.println(String.valueOf(somme));
    }

    public static String tenToBin(int value) {
        ArrayList<String> listeStringBaseTen = new ArrayList<>();

        int diviseur = 2;
        int reste = 0;
        int resultat = 0;
        do {
            resultat = value / diviseur;
            reste = value % diviseur;
            listeStringBaseTen.add(String.valueOf(reste));
            value = resultat;
        } while (resultat != 0);
        Collections.reverse(listeStringBaseTen);
        String resultString = String.join("", listeStringBaseTen);
        return resultString;
    }

    public static int voyelles(String valeur) {
        char[] tabVoyelle = {'a', 'e', 'i', 'o', 'u', 'y'};
        char[] tabValue = new char[valeur.length()];
        for (int k = 0; k < valeur.length(); k++) {
            tabValue[k] = valeur.charAt(k);
        }
        int resultat = 0;
        for (int i = 0; i < tabValue.length - 1; i++) {
            for (int j = 0; j < tabVoyelle.length; j++) {
                if (tabValue[i] == tabVoyelle[j]) {
                    resultat++;
                }
            }
        }
        return resultat;
    }

    public static int majuscule(String value) {
        int[] tabAsciiMaj = new int[26];
        int j = 0;
        for (int i = 65; i < 91; i++) {
            tabAsciiMaj[j] = i;
            j++;
        }

        int resultat = 0;
        for (int k = 0; k < tabAsciiMaj.length; k++) {
            for (int l = 0; l < value.length(); l++) {
                int ascii = (int) value.charAt(l);
                if (tabAsciiMaj[k] == ascii) {
                    resultat++;
                }
            }


        }
//        System.out.println(Arrays.toString(tabAsciiMaj));
        return resultat;
    }

    public static String minToMaj(String value) {
        char[] tabConvert = new char[value.length()];
        for (int l = 0; l < value.length(); l++) {
            int ascii = (int) value.charAt(l);
            tabConvert[l] = (char) (ascii - 32);
        }
        return Arrays.toString(tabConvert);
    }

    public static String fusion() {
        int[] array1 = {1, 3, 4, 5};
        int[] array2 = {2, 4, 6, 8};
        int[] res = new int[array1.length + array2.length];
        int i = 0;
        for (int valeur : array1) {
            res[i] = valeur;
            i++;
        }
        for (int valeur : array2) {
            res[i] = valeur;
            i++;
        }
        Arrays.sort(res);
        return Arrays.toString(res);
    }

    public static String palindrome(String value) {
        char[] tabCharValue = new char[value.length()];

        int valeurMedianne;
        if (value.length() % 2 != 0) {
            valeurMedianne = tabCharValue.length / 2;
            char[] tabCharWithoutMedianne = new char[tabCharValue.length - 1];
            int k = 0;
            for (int j = 0; j < tabCharValue.length; j++) {
                if (j != valeurMedianne) {
                    tabCharWithoutMedianne[k] = value.charAt(j);
                    k++;
                }
            }
            int inverseTotalChar = tabCharWithoutMedianne.length - 1;
            for (int l = 0; l < tabCharValue.length - 1; l++) {
                if (tabCharWithoutMedianne[l] != tabCharWithoutMedianne[inverseTotalChar]) {
                    return "Not a palindrome";
                }
                inverseTotalChar--;

            }
            return "Its a palindrome";
        } else {
            for (int i = 0; i < value.length(); i++) {
                tabCharValue[i] = value.charAt(i);
            }
            return Arrays.toString(tabCharValue);
        }

    }

    public static String standardise(String valeur) {
        ArrayList<Character> resultat = new ArrayList<>();
        ArrayList<Character> tabValeur = new ArrayList<>();
        ArrayList<Character> banChar = new ArrayList<>();
        for (int i = 32; i < 65; i++) {
            banChar.add((char) i);
        }
        for (int i = 91; i < 97; i++) {
            banChar.add((char) i);
        }
        for (int i = 0; i < valeur.length(); i++) {
            tabValeur.add((char) valeur.charAt(i));
        }

        int j = 0;
        for (int i = 0; i < tabValeur.size(); i++) {
            if (tabValeur.get(i) >= 97 && tabValeur.get(i) < 123) {
                resultat.add(tabValeur.get(i));
            } else if (tabValeur.get(i) >= 65 && tabValeur.get(i) < 91) {
                int majToMin = (int) tabValeur.get(i) + 32;
                resultat.add((char) majToMin);
            } else if (tabValeur.get(i) == 130 || tabValeur.get(i) >= 136 && tabValeur.get(i) < 139) {
                resultat.add((char) 101);
            } else if (tabValeur.get(i) >= 131 && tabValeur.get(i) < 134 || tabValeur.get(i) == 142 || tabValeur.get(i) == 143) {
                resultat.add((char) 97);
            } else if (tabValeur.get(i) == 'é' || tabValeur.get(i) == 'ë' || tabValeur.get(i) == 'É') {
                resultat.add((char) 101);
            }
        }
        char[] tab = new char[resultat.size()];
        for (int i = 0; i < resultat.size(); i++) {
            tab[i] = resultat.get(i);
        }

        return String.valueOf(tab);
    }

    public static String palindromeTwo(String valeur) {
        String formatString = standardise(valeur);
        int stringLength = formatString.length() - 1;
        System.out.println(formatString);

        for (int i = 0; i < formatString.length(); i++) {
            System.out.println(stringLength + ":" + i);
            if (formatString.charAt(i) != formatString.charAt(stringLength)) {
                System.out.println(formatString.charAt(i) + ":" + formatString.charAt(stringLength));
                return "ce nest pas un palindrome";
            }
            stringLength--;
        }
        return "cest un palindrome";
    }

    public static String cardVerifier(String value) {
        String stringWithoutSpaces = value.replaceAll("\\s", "");
        int[] tabInt = new int[stringWithoutSpaces.length()];
        int somme = 0;
//        insere tous les chiffres dans tabInt[]
        for (int i = 0; i < tabInt.length; i++) {
            tabInt[i] = Character.getNumericValue(stringWithoutSpaces.charAt(i));
        }
//        multiplie tous les chiffres impairs
        for (int i = 0; i < tabInt.length; i++) {
            if (i % 2 == 0) {
                tabInt[i] = tabInt[i] * 2;
            }
        }
//        si deux chiffres, les additionner
        for (int i = 0; i < tabInt.length; i++) {
            if (i % 2 == 0) {
                if (tabInt[i] >= 10) {
                    tabInt[i] = tabInt[i] / 10 + tabInt[i] % 10;
                }
            }
        }
//        additionner tous les chiffres
        for (int i = 0; i < tabInt.length; i++) {
            somme += tabInt[i];
        }

        if (somme % 10 == 0) {
            return "Numéro valide";
        } else {
            return "Numéro invalide";
        }
    }

    //    public static int[] glouton(int value){
//        int[] tabPieces = {200, 100, 50, 20, 10, 5, 2, 1};
//        int[] tabSortie = {0,0,0,0,0,0,0,0};
//        int[] tabValueMultiply = new int[String.valueOf(value).length()];
//        if(String.valueOf(value).length() == 2){
//
//                tabValueMultiply[0] =  Character.getNumericValue(String.valueOf(value).charAt(0))*10 ;
//                tabValueMultiply[1] = Character.getNumericValue(String.valueOf(value).charAt(1));
//
//
//        }
//        System.out.println(Arrays.toString(tabValueMultiply) );
//        int[] tabModulo = new int[8];
//        int i = 0;
//        for (int element : tabPieces){
//            tabModulo[i] = tabValueMultiply[0] % element;
//            i++;
//        }
//        int j = 0;
//        for(int element : tabModulo){
//            if(element == 0){
//                tabSortie[j] += 1;
//                break;
//            }
//            j++;
//        }
//        System.out.println(Arrays.toString(tabModulo) );
//        return tabSortie;
//    }
    public static String glouton(int value) {
        int[] tabPieces = {200, 100, 50, 20, 10, 5, 2, 1};
        int[] tabSortie = {0, 0, 0, 0, 0, 0, 0, 0};

        for (int j = 0; j < tabPieces.length; j++) {
            while (value >= tabPieces[j]) {
                value = value - tabPieces[j];
                tabSortie[j]++;
            }
        }

        return Arrays.toString(tabSortie);
    }

    static int[] tableau = {2, 7, 12, 15, 22, 18, 25, 7, 8};

    public static void triRecursif(int value, int somme) {

        if (value == 0 && somme == 0) {
            somme = tableau[0];
            value++;
            triRecursif(value, somme);
        } else if (value < tableau.length) {
            System.out.println(somme);
            somme += tableau[value];
            value++;
            triRecursif(value, somme);
        } else {
            System.out.println(somme);
        }
    }

    public static String encrypt(String value, int shiftValue, String direction) {
        char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        ArrayList<Character> resultat = new ArrayList<>();

        for (int i = 0; i < value.length(); i++) {
            int shiftIndex;
            int alphaIndex = value.charAt(i) - 'a';

            if(direction.equals("RIGHT")){
                shiftIndex = (alphaIndex + shiftValue) % alphabet.length;
                System.out.println("alphaIndex : " + alphaIndex + " shiftValue : " + shiftValue + " shiftIndex : " + shiftIndex);
            }else{
                shiftIndex = (alphaIndex - shiftValue) % alphabet.length;
            }

            resultat.add((char) (shiftIndex + 'a'));

        }
        return resultat.toString();
    }

    public static String decrypt(String value, int shiftValue, String direction){
        char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        ArrayList<Character> resultat = new ArrayList<>();
        String resultatString = "";
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < value.length(); i++) {
            int shiftIndex;
            int alphaIndex = value.charAt(i) - 'a';
            if(direction.equals("RIGHT")){
                shiftIndex = (alphaIndex - shiftValue) % alphabet.length;
            }else{
                shiftIndex = (alphaIndex + shiftValue) % alphabet.length;
            }

            resultat.add((char) (shiftIndex + 'a'));

        }

        for(char element : resultat){
            builder.append(element);
        }

        return builder.toString();
    }

    /**
     * fonction de tri par selection
     */
    public static void triSelection(){
        int[] tab = {12,24,10,5,5,7,54,8,65,41,2,3};
        int[] tabTemp = new int[tab.length];
        int max = 0;
        int k = tabTemp.length - 1;
        for(int i=0; i<tab.length - 1 ; i++){
            int index = i;
            for(int j= i + 1; j<tab.length; j++){

                if(tab[j]<tab[index]){
                    index = j;
                }
            }
            int min = tab[index];
            tab[index] = tab[i];
            tab[i] = min;
        }
        System.out.println(Arrays.toString(tab));
    }
    static int[] triBulle(int[] tab)
    {
        int taille = tab.length;
        int tmp = 0;
        for(int i=0; i < taille; i++)
        {
            for(int j=1; j < (taille-i); j++)
            {
                if(tab[j-1] > tab[j])
                {
                    tmp = tab[j-1];
                    tab[j-1] = tab[j];
                    tab[j] = tmp;
                }

            }
        }
        return tab;
    }
    static int[] triBulle2(int[] tab)
    {
        int taille = tab.length;
        int tmp = 0;
        for(int i=0; i < taille; i++)
        {
            for(int j=1; j < (taille-i); j++)
            {
                if(tab[j-1] > tab[j])
                {
                    tmp = tab[j-1];
                    tab[j-1] = tab[j];
                    tab[j] = tmp;
                }

            }
        }
        return tab;
    }

    static String dichotomique(int valueToFind){
        int[] tab = {8,12,14,19,21,22,24,32,53,53,58,70,74,80,85,87,89,89,94,96};
        int mid = tab.length / 2;
        String resultat = "";
        System.out.println(tab[mid] + " " + mid);
        if(valueToFind == tab[mid]){
        } else if (valueToFind > tab[mid]) {
            for(int i=mid; i<tab.length; i++){
                if(valueToFind == tab[i]){
                    resultat = "Trouvé à l'index : " + i;
                    break;
                }else {
                    resultat = "Non trouvé";
                }
            }
        }else{
            for(int i=mid; i>0; i--){
                if(valueToFind == tab[i]){
                    resultat = "Trouvé à l'index : " + i;
                    break;
                }else {
                    resultat = "Non trouvé";
                }
            }
        }
        return resultat;
    }

    public static String kPlusGrand(int[] tab, int k){
        int[] tableauTrie = triBulle(tab);
        int[] tabSortie = new int[k];
        int j = 0;
        for(int i=tab.length - k; i<tab.length; i++){
            tabSortie[j] = tableauTrie[i];
            j++;
        }
        return Arrays.toString(tabSortie);
    }

    public static int[] mastermindVerif(String value){
        int[] tabEntree = new int[4];
        for(int i=0; i<value.length(); i++){
            tabEntree[i] = Character.getNumericValue(value.charAt(i));
        }
        int[] soluce = {2,7,0,4};
        int[] verif = new int[4];

        for(int i = 0; i<=3 ; i++){
            if(tabEntree[i] == soluce[i]){
                verif[i] = 5;
            }
        }

        for(int iSoluce = 0; iSoluce<3; iSoluce++){
            for(int jEntree = 0; jEntree<3; jEntree++){
                if(verif[iSoluce] != 5){
                    if(tabEntree[jEntree] == soluce[iSoluce]){
                        verif[jEntree] = 1;
                    }
                }
            }
        }
        System.out.println(Arrays.toString(verif));
        return verif;
    }

    public static void mastermind(int nbPartie, int nbPoints){

        int isValid = 0;
        int malPlace = 0;
        int j = 1;
        Scanner scan = new Scanner(System.in);

        if(nbPartie == 9){
            System.out.println("Vous avez perdu !");
        }else{
            if(nbPoints == 4){
                System.out.println("Vous avez gagné en "+ (nbPartie - 1) +" coup(s) !");
            }else{
                System.out.println("Proposition " + nbPartie + " : ");
                String premiere = scan.next();
                int[] reponse = mastermindVerif(premiere);
                for(int i=0; i<reponse.length; i++){
                    if(reponse[i] == 1){
                        malPlace++;
                    }else if(reponse[i] == 5){
                        isValid++;
                    }
                }
                System.out.println("Vous avez "+isValid+" bien placé(s) et "+malPlace+" mal placé(s)!");
                nbPartie++;
                mastermind(nbPartie, isValid);
            }
        }
    }

    public static String inverse(String value){
        char[] tabResChar = new char[value.length()];
        int j = 0;
        for(int i = value.length() - 1; i >= 0; i--){
            tabResChar[i] = value.charAt(j);
            j++;
        }
        return Arrays.toString(tabResChar);
    }

    public static String camelCase(String value){
        char[] tabChar = new char[value.length()];
        ArrayList<Character> listeChar = new ArrayList<>();

        for(int i = 0; i<value.length(); i++){
            tabChar[i] = value.charAt(i);
            listeChar.add(value.charAt(i));
        }
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i<listeChar.size(); i++){
            if(listeChar.get(i) == 32){
                listeChar.add(i+1, (char) (listeChar.get(i+1)-32));
                listeChar.remove(i+2);
            }else{
                builder.append(listeChar.get(i));
            }
        }
        listeChar.removeIf(n -> (n == 32));
        return builder.toString();
    }

    public static String localisation(String reference, String value){
        ArrayList<Character> listeRef = new ArrayList<>();
        ArrayList<Character> listeValue = new ArrayList<>();
        ArrayList<Integer> listeIndex = new ArrayList<>();
        int[] tabIndex = new int[value.length()];
        for(int i = 0; i<reference.length(); i++){
            listeRef.add(reference.charAt(i));
        }
        for(int i = 0; i<value.length(); i++){
            listeValue.add(value.charAt(i));
        }
        int totalBonChar = 0;
        int bonIndex = 0;
        int j = 0;
        for(int i = 0; i<listeRef.size(); i++){
            char c = value.charAt(j);
            if(listeRef.get(i) == c){
                totalBonChar++;
                listeIndex.add(i);
                j++;
                if(totalBonChar == value.length()){
                    return "1er index : " + Collections.min(listeIndex) + " 2eme index : " + Collections.max(listeIndex);
                }

            }else{
                totalBonChar = 0;
            }
        }
        return "nop";
    }

    public static void pyramideV1(int value){
        int i = 0;
        ArrayList<Character> tabStars = new ArrayList<Character>();

        while (i <= value){
            tabStars.add('*');
            System.out.println(tabStars.toString());
            i++;
        }
    }
    public static void pyramideV2(int value){
        int i = 0;
        ArrayList<Character> tabStars = new ArrayList<Character>();
        boolean isDesc = false;

        while (i <= value){

                tabStars.add('*');
                System.out.println(tabStars.toString());
                i++;

        }
        i--;
        while (i > 0){
            tabStars.remove(i);
            System.out.println(tabStars.toString());
            i--;
        }
    }

    public static void pendu(String mot){
        System.out.println("Bienvenue dans le jeu ou vous risquez de perdre la tête");

        ArrayList<Character> listChar = new ArrayList<Character>();
        for(int i = 0; i<mot.length(); i++){
            listChar.add(mot.charAt(i));
        }
        char[] soluce = new char[mot.length()];
        char[] affichage = new char[mot.length()];

        for(int i = 0; i<listChar.size(); i++){
            soluce[i] = mot.charAt(i);
        }

        for(int i = 0; i<listChar.size(); i++){
            soluce[i] = mot.charAt(i);
            affichage[i] = '-';
        }
        System.out.println(Arrays.toString(affichage));
        verifPendu(1, 0, soluce, affichage, 0);
    }
    public static void verifPendu(int tour, int bonChar, char[] soluce, char[] affichage, int wrong){
        Scanner scan = new Scanner(System.in);
        System.out.println("Choix d'un lettre : ");
        String lettreString = scan.next();
        int bonCharReset = 0;
        char lettreChar = lettreString.charAt(0);
        for(int i = 0; i<soluce.length; i++){
            if(soluce[i] == lettreChar){
                affichage[i] = lettreChar;
                bonChar++;
                bonCharReset++;
            }
        }
        if(bonCharReset > 0){
            System.out.println("Bien joué !");
        }
        if(bonCharReset == 0){
            wrong++;
            switch (wrong){
                case 1:
                    System.out.println("Le bourreau sort de la prison");
                    break;
                case 2:
                    System.out.println("Le bourreau monte sur l'estrade");
                    break;
                case 3:
                    System.out.println("Vous sortez de la prison en hurlant que vous êtes innocent");
                    break;
                case 4:
                    System.out.println("Vous hurelez que vous souhaiteriez baiser le procureur tout en montant sur l'estrade");
                    break;
                case 5:
                    System.out.println("Le bourreau caresse les épissures de la corde, elle a l'air en bon état");
                    break;
                case 6:
                    System.out.println("Vous pleurez votre mère pendant que le bourreau vous met la corde au cou");
                    break;
                case 7:
                    System.out.println("Le bourreau tend la courde, vous sentez avez du mal a respirer");
                    break;
                case 8:
                    System.out.println("Vous êtes mort, le sang irrigue votre penis dans une érection morbide, c'est tout ce que les gens retiendront de vous");
                    break;
            }
        }
        System.out.println(Arrays.toString(affichage));
        if (wrong == 8) {
            System.out.println("Vous êtes mort, le sang irrigue votre penis dans une érection morbide, c'est tout ce que les gens retiendront de vous");
        } else if(bonChar == soluce.length){
            System.out.println("Un spectateur vous jette du pain, le pain attérri sur la potence, une armée de pigeons viens le picorer, rongeant la corde. Elle céde, vous êtes libre !");
        }else{
            tour++;
            verifPendu(tour, bonChar, soluce, affichage, wrong);
        }


    }
}